package com.onaemo.tatteam.service.pushui.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.ServletRequestUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class Oauth2Filter extends AbstractAuthenticationProcessingFilter {

	public Oauth2Filter() {
		super("/login");
	}

	private final String authHeader = "Authorization";

	@Value("${security.oauth2.resource.jwt.key-value}")
	String publicKey;


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

		log.info("############################## Oauth2Filter request uri :: " + request.getRequestURI() + "##############################");


		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			log.info("auth.getName() :: " + auth.getName());
		}
		String token = ServletRequestUtils.getStringParameter(request, "Authorization");

		ObjectMapper objectMapper = new ObjectMapper();

		Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));

		Map<String, Object> claims = objectMapper.readValue(jwt.getClaims(), Map.class);

		for( String key : claims.keySet() ){
			log.info( "claims [{}] :: {}", key, claims.get(key));
		}

		List<GrantedAuthority> authorities = new ArrayList<>();

		ArrayList<String> list = (ArrayList) claims.get("authorities");

		for(String item : list) {
			log.info(item);
			authorities.add(new SimpleGrantedAuthority(item));
		}
		authorities.add(new SimpleGrantedAuthority("ROLE_MANAGER"));
		authorities.add(new SimpleGrantedAuthority("ROLE_DEVELOPER"));


		AuthVO authVO = new AuthVO();
		authVO.setClientId(claims.get("client_id").toString());
		authVO.setUsername(claims.get("user_name").toString());
		authVO.setName(claims.get("user_name").toString());
		authVO.setRole(claims.get("user_role").toString());
		authVO.setTokenValue(token);

		authVO.setAuthorities(authorities);

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(authVO, "", authVO.getAuthorities());
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//		authentication.setDetails(authVO);

		return getAuthenticationManager().authenticate(authentication);

	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)	throws IOException, ServletException {
		String token = ServletRequestUtils.getStringParameter(request, "Authorization");
		SecurityContextHolder.getContext().setAuthentication(authResult);

		response.sendRedirect("/?Authorization="+token);



//		super.successfulAuthentication(request, response, chain, authResult);

		// As this authentication is in HTTP header, after success we need to continue the request normally
		// and return the response as if the resource was not secured at all

//		chain.doFilter(request, response);

	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
		log.info("unsuccessfulAuthentication {} ", request.getRequestURI());
		log.info(failed.getMessage());
//		log.error("",failed.getStackTrace());


//		super.unsuccessfulAuthentication(request, response, failed);
	}
}
