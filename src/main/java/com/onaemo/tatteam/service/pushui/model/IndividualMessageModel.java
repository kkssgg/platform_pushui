package com.onaemo.tatteam.service.pushui.model;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IndividualMessageModel {
	private String clientId;
	private int messageType;
	private String messageTitle;
	private String messageBody;
	private Map<String, Object> messageData;
	private String userName;

}
