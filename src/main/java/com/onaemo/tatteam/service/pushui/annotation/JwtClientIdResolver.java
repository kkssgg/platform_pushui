package com.onaemo.tatteam.service.pushui.annotation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onaemo.tatteam.service.pushui.security.AuthVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.server.ResponseStatusException;
import org.thymeleaf.util.StringUtils;

import java.util.Map;

@Slf4j
public class JwtClientIdResolver implements HandlerMethodArgumentResolver {
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType().isAssignableFrom(JwtCustom.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

		JwtCustom jwtCustom = new JwtCustom();

		Authentication authentication = (Authentication) webRequest.getUserPrincipal();

		AuthVO authVO = null;
		String Authorization = webRequest.getHeader("Authorization") == null ? "" : webRequest.getHeader("Authorization").toString();


		if(authentication == null && StringUtils.isEmpty(Authorization)) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}else if(authentication != null){
			authVO = (AuthVO)authentication.getPrincipal();

			jwtCustom.setClientId(authVO.getClientId());
			jwtCustom.setUserId(authVO.getName());
		}else if(!StringUtils.isEmpty(Authorization)){
			ObjectMapper objectMapper = new ObjectMapper();

			/**
			 * json web token 데이터 디코딩하여 json web token 값 상세 정보 가져오기
			 * - JwtHelper.decode로 원문 디코딩, 위에서 구한 Authentication 상세정보에 tokenValue에 json web token 인코딩 값이 있음
			 * - Jwt 상세정보를 Map으로 변환하여 원하는 데이터를 가져옴
			 */
			String token = Authorization.substring(7);
			Jwt jwt = JwtHelper.decode(token);

			Map<String, Object> claims = objectMapper.readValue(jwt.getClaims(), Map.class);

//			Iterator<String> keys = claims.keySet().iterator();
//			while(keys.hasNext()){
//				String key = keys.next();
//				log.info("{} :: {}", key, claims.get(key));
//			}

			/**
			 * Authentication 사용자 아이디를 가져옴
			 * 2019.07.07 변경됨
			 * 푸시 서비스만 사용하는 클라이언트 앱일 경우, 인증서버를 통해서 사용자 인증을 사용하지 않을 수 있다.
			 * 이때는 클라이언트 앱만 인증서버를 통해서 인증하는 경우가 발생한다. 그런경우를 위해서 사용자구분아이디는 무조건 요청 파라미터로 받아야 한다.
			 */


			/**
			 * Jwt 상세정보에서 클라이언트 아이디를 가져옴
			 */
			jwtCustom.setClientId((String) claims.get("client_id"));
			jwtCustom.setUserId((String) claims.get("user_name"));
		}


		return jwtCustom;
	}
}

