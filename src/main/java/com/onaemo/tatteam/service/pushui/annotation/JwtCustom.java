package com.onaemo.tatteam.service.pushui.annotation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtCustom {
	private String clientId;
	private String userId;
}
