package com.onaemo.tatteam.service.pushui.rest.contorller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.onaemo.tatteam.service.pushui.annotation.JwtClientId;
import com.onaemo.tatteam.service.pushui.annotation.JwtCustom;
import com.onaemo.tatteam.service.pushui.db.service.OnaemoPlatformProjectService;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import java.util.*;

@Slf4j
@RestController("rest.user.contorller")
public class UserRestController {

	@Value("${service.push.url}") String PUSH_SERVICE_URL;

	@Autowired
	OnaemoPlatformProjectService onaemoPlatformProjectService;

	/**
	 * @brief 푸시 사용자 목록
	 * @param jwtCustom
	 * @param token
	 * @return
	 */
	@GetMapping("/rest/push/user/list")
	public ResponseEntity<Map<String, Object>> getPushUsers(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                        @RequestParam("projectId") String projectId, @RequestParam("userName") String userName,
	                                                        @RequestParam("pageNum") int pageNum, @RequestParam("scaleNum") int scaleNum){
		Map<String, Object> result = new HashMap<>();
		try{

			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);


			Map<String, Object> requestBody = new HashMap<>();
			requestBody.put("clientId","onaemoclientId");
			requestBody.put("page", pageNum);
			requestBody.put("scale", scaleNum);
			if(!StringUtils.isEmpty(userName)) requestBody.put("userName", userName);


			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);


			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/target/info/list", HttpMethod.POST, httpEntity, String.class);

				if(responseEntity.getStatusCode() == HttpStatus.OK){
					Gson gson = new Gson();
					Map<String, Object> data = gson.fromJson(responseEntity.getBody().toString(), Map.class);

					List<Map<String, Object>> targetModelList = objectMapper.convertValue(data.get("targetModelList"), List.class);

					targetModelList.forEach(item -> {
						item.remove("targetToken");
					});

					log.info(data.toString());

					return new ResponseEntity<>(data, HttpStatus.OK);
				}else{
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}

			} catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}



		}catch(Exception e){
			log.error("ERROR /rest/push/users", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	/**
	 * @brief 푸시 사용자 삭제
	 * @param jwtCustom
	 * @param token
	 * @param clientId
	 * @param userName
	 * @return
	 */
	@PostMapping("/rest/push/user/del")
	public ResponseEntity<Map<String, Object>> delUser (@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                    @RequestParam("clientId") String clientId, @RequestParam("userName") String userName){
		try{
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			Map<String, Object> requestBody = new HashMap<>();
			requestBody.put("clientId", clientId);
			requestBody.put("userName", userName);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/target/info/delete", HttpMethod.DELETE, httpEntity, String.class);


				if(responseEntity.getStatusCode() == HttpStatus.OK){
					return new ResponseEntity<>(HttpStatus.OK);
				}else{
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}

			} catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}

		}catch(Exception e){
			log.error("ERROR /rest/push/user/del",e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}





}
