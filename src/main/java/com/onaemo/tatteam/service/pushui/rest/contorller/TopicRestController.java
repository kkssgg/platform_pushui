package com.onaemo.tatteam.service.pushui.rest.contorller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.onaemo.tatteam.service.pushui.annotation.JwtClientId;
import com.onaemo.tatteam.service.pushui.annotation.JwtCustom;
import com.onaemo.tatteam.service.pushui.db.service.OnaemoPlatformProjectService;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController("rest.topic.controller")
public class TopicRestController {

	@Value("${service.push.url}") String PUSH_SERVICE_URL;

	@Autowired
	OnaemoPlatformProjectService onaemoPlatformProjectService;

	/**
	 * @brief 플렛폼 PUSH topic Subscriber
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @return
	 */
	@GetMapping("/rest/push/topic/subscriber")
	public ResponseEntity<Map<String, Object>> getPushTopicSearch(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                            @RequestParam("projectId") String projectId){
		Map<String, Object> result = new HashMap<>();
		try{


			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);


			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");



			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/search/topic/subscriber", HttpMethod.POST, httpEntity, String.class);

			if(responseEntity.getStatusCode() == HttpStatus.OK){
//				Gson gson = new Gson();
//				Map<String, Object> data = gson.fromJson(responseEntity.getBody().toString(), Map.class);
				List<Map<String, Object>> data = objectMapper.readValue(responseEntity.getBody().toString(), List.class);
				result.put("items",data);
				return new ResponseEntity<>(result, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(responseEntity.getStatusCode());
			}

		}catch(Exception e){
			log.error("ERROR /rest/push/topic/subscriber", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}


	/**
	 * @brief 플렛폼 PUSH Topic 이력 목록
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @param pageNum
	 * @param scaleNum
	 * @param topicName
	 * @param agentResult
	 * @param messageType
	 * @param targetAck
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@GetMapping("/rest/push/topic/list")
	public ResponseEntity<Map<String, Object>> getPushTopicList(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                            @RequestParam("projectId") String projectId, @RequestParam("pageNum") int pageNum, @RequestParam("scaleNum") int scaleNum,
	                                                            @RequestParam("topicName") String topicName, @RequestParam("agentResult") String agentResult, @RequestParam("messageType") String messageType,
	                                                            @RequestParam("targetAck") String targetAck, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
		log.info("################## PUSH TOPIC LIST ##################");
		try{


			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);


			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");
			requestBody.put("page", pageNum);
			requestBody.put("scale", scaleNum);
			if(!StringUtils.isEmpty(topicName)) requestBody.put("topicName", topicName);
			if(!StringUtils.isEmpty(agentResult)) requestBody.put("agentResult", agentResult);
			if(!StringUtils.isEmpty(messageType)) requestBody.put("messageType", messageType);
			if(!StringUtils.isEmpty(targetAck)) requestBody.put("targetAck", targetAck);
			if(!StringUtils.isEmpty(startDate)) requestBody.put("startDate", startDate.replaceAll("-",""));
			if(!StringUtils.isEmpty(endDate)) requestBody.put("endDate", endDate.replaceAll("-",""));

			log.info(requestBody.toString());


			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);


			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/search/topic/message", HttpMethod.POST, httpEntity, String.class);

				if(responseEntity.getStatusCode() == HttpStatus.OK){
					log.info(responseEntity.getBody().toString());
					Map<String, Object> data = objectMapper.readValue(responseEntity.getBody().toString(),Map.class);
					return new ResponseEntity<>(data, HttpStatus.OK);
				}else{
					log.info("PUSH TOPIC LIST HTTP_STATUS[{}]", responseEntity.getStatusCode());
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}
			}catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}
		}catch(Exception e){
			log.error("ERROR /rest/push/topic/list", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	/**
	 * @brief 플렛폼 PUSH Topic 전송
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @param messageType
	 * @param messageTitle
	 * @param messageBody
	 * @param messageData
	 * @param topicNameList
	 * @return
	 */
	@PostMapping("/rest/push/topic/send")
	public ResponseEntity<Map<String, Object>> sendMulticast(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                         @RequestParam("projectId") String projectId, @RequestParam(value="condition", required = false) String condition,
	                                                         @RequestParam("messageType") int messageType, @RequestParam("messageTitle") String messageTitle, @RequestParam("messageBody") String messageBody,
	                                                         @RequestParam(value="messageData", required = false) String messageData, @RequestParam("topicNameList") String topicNameList){

		try{
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);

			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");
//			if(condition != null){
//				requestBody.put("condition","condition");
//			}
			requestBody.put("messageType", messageType);
			requestBody.put("messageTitle", messageTitle);
			requestBody.put("messageBody", messageBody);
			requestBody.put("topicName", topicNameList);

			if(messageData != null){
				Map< String, Object> map = objectMapper.readValue(messageData, new TypeReference<Map<String, String>>(){});
				requestBody.put("messageData", map);
			}

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL + "/onaemomessage/api/v1/message/topic", HttpMethod.POST, httpEntity, String.class);

				if (responseEntity.getStatusCode() == HttpStatus.OK) {
					return new ResponseEntity<>(HttpStatus.OK);
				} else {
					log.info("PUSH TOPIC SEND HTTP_STATUS[{}]", responseEntity.getStatusCode());
					log.info(responseEntity.getBody().toString());
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}

			} catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}

		}catch(Exception e){
			log.error("/rest/push/topic/send",e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
