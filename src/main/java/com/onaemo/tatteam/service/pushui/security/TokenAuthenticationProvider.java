package com.onaemo.tatteam.service.pushui.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class TokenAuthenticationProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {


		log.info("TokenAuthenticationProvider");

		AuthVO authVo = (AuthVO) authentication.getPrincipal();

//		return new UsernamePasswordAuthenticationToken(authVo.getName(), "" , authVo.getAuthorities());
		return new UsernamePasswordAuthenticationToken(authVo, "" , authVo.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
