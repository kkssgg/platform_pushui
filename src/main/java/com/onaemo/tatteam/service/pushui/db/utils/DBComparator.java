package com.onaemo.tatteam.service.pushui.db.utils;


public class DBComparator {

	public static boolean isEmpty(Object obj){
		return obj == null || "".equals(obj.toString().trim()) || "0".equals(obj.toString().trim()) || "0.0".equals(obj.toString().trim());
	}
	
	public static boolean isNotEmpty(Object s){
		return !isEmpty(s);
    }
	
	public static boolean isEmpty2(Object s){
		return s==null || "".equals(s.toString().trim()) || "-1".equals(s.toString().trim());
    }
	
	public static boolean isNotEmpty2(Object s){
		return !isEmpty2(s);
    }
}
