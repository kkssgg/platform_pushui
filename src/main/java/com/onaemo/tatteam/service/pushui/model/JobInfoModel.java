package com.onaemo.tatteam.service.pushui.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobInfoModel {
	private String clientId;
	private String cronExpression;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	private Date startTime;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	private Date endTime;

	private boolean isCronJob;
	private boolean isDurable;
	private Integer repeatTime;
	private Integer jobClassId;
	private JobData jobData;
	private String jobGroup;
	private String jobName;
	private String jobStatus;
	private String regDate;
	private String modDate;
	private Integer scheduleJobId;
}
