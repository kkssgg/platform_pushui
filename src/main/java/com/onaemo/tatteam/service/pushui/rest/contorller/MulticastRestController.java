package com.onaemo.tatteam.service.pushui.rest.contorller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onaemo.tatteam.service.pushui.annotation.JwtClientId;
import com.onaemo.tatteam.service.pushui.annotation.JwtCustom;
import com.onaemo.tatteam.service.pushui.db.service.OnaemoPlatformProjectService;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController("rest.mulitcast.controller")
public class MulticastRestController {

	@Value("${service.push.url}") String PUSH_SERVICE_URL;

	@Autowired
	OnaemoPlatformProjectService onaemoPlatformProjectService;


	/**
	 * @brief 플렛폼 PUSH 단체 메세지 전송
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @param messageType
	 * @param messageTitle
	 * @param messageBody
	 * @param messageData
	 * @param userNameList
	 * @return
	 */
	@PostMapping("/rest/push/multicast/send")
	public ResponseEntity<Map<String, Object>> sendMulticast(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                         @RequestParam("projectId") String projectId,
	                                                         @RequestParam("messageType") int messageType, @RequestParam("messageTitle") String messageTitle, @RequestParam("messageBody") String messageBody,
	                                                         @RequestParam(value="messageData", required = false) String messageData, @RequestParam("userNameList") String userNameList){

		try{
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);


			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");
			requestBody.put("messageType", messageType);
			requestBody.put("messageTitle", messageTitle);
			requestBody.put("messageBody", messageBody);
			requestBody.put("userNameList", userNameList.split(","));

			if(messageData != null) {
				Map<String, Object> map = objectMapper.readValue(messageData, new TypeReference<Map<String, String>>() {});
				requestBody.put("messageData", map);
			}

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/message/multicast", HttpMethod.POST, httpEntity, String.class);

				if(responseEntity.getStatusCode() == HttpStatus.OK){
					return new ResponseEntity<>(HttpStatus.OK);
				}else{
					log.info("PUSH INDIVIDUAL LIST HTTP_STATUS[{}]", responseEntity.getStatusCode());
					log.info(responseEntity.getBody().toString());
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}

			}catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}

		}catch(Exception e){
			log.error("/rest/push/mulitcast/send",e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
