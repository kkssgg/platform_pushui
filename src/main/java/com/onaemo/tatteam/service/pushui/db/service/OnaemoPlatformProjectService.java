package com.onaemo.tatteam.service.pushui.db.service;

import com.onaemo.tatteam.service.pushui.db.mapper.OnaemoPlatformProjectMapper;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("db.onaemo.platform.project")
public class OnaemoPlatformProjectService {

	@Autowired
	OnaemoPlatformProjectMapper mapper;

	public int getTotalCount(Map<String, Object> params){
		return mapper.getTotalCount(params);
	}

	public List<OnaemoPlatformProject> getOnaemoPlatformProjectList(Map<String, Object> params){
		return mapper.getOnaemoPlatformProjectList(params);
	}

	public OnaemoPlatformProject getOnaemoPlatformProject (String projectId){
		return mapper.getOnaemoPlatformProject(projectId);
	}
}
