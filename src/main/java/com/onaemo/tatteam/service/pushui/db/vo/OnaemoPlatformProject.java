package com.onaemo.tatteam.service.pushui.db.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class OnaemoPlatformProject {
	private String onaemoProjectId;
	private String projectName;
	private String client_id;
	private Date addDate;
	private Date modDate;
	private String userId;
	private OauthClientDetails oauthClientDetails;
}
