package com.onaemo.tatteam.service.pushui.db.mapper;

import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OnaemoPlatformProjectMapper {

	/**
	 * @brief 플랫폼 프로젝트 갯수
	 * @param params
	 * @return
	 */
	public int getTotalCount(Map<String, Object> params);

	/**
	 * @brief 플랫폼 프로젝트 목록
	 * @param params
	 * @return
	 */
	public List<OnaemoPlatformProject> getOnaemoPlatformProjectList(Map<String, Object> params);

	/**
	 * @brief 플랫폼 프로젝트
	 * @param projectId
	 * @return
	 */
	public OnaemoPlatformProject getOnaemoPlatformProject(@Param("projectId") String projectId);



}
