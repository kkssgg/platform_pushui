package com.onaemo.tatteam.service.pushui.security;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Slf4j
@Configuration
@EnableWebSecurity
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("security.oauth2.resource.key-value")
	String key;

	@Autowired
	TokenAuthenticationProvider tokenAuthenticationProvider;

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/**/img/**");
	}


	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
		.and()
//			.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
//			.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(authenticationTokenFilterBean(), BasicAuthenticationFilter.class)

			.authorizeRequests()
				.antMatchers("/", "/login", "/oauth/user/info2", "/callback", "/favicon.ico", "/css/**", "/js/**", "/vendors/**", "/fonts/**").permitAll()
//		.and()
//			.authorizeRequests()
//				.antMatchers("/platform/**").hasAuthority("ROLE_MANAGER")
		.and()
			.authorizeRequests()
				.antMatchers("/app/**").hasAuthority("ROLE_DEVELOPER")
//		.and().authorizeRequests().anyRequest().permitAll()
		.and().csrf().disable();


	}

	@Bean
	public Oauth2Filter authenticationTokenFilterBean() throws Exception {
		Oauth2Filter authenticationTokenFilter = new Oauth2Filter();
		authenticationTokenFilter.setAuthenticationManager(authenticationManager());
//		authenticationTokenFilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
		return authenticationTokenFilter;
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(tokenAuthenticationProvider);
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource2() {

		log.info("CorsConfigurationSource");
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.setAllowCredentials(true);
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		config.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}
}