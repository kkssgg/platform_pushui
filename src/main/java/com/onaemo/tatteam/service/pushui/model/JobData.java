package com.onaemo.tatteam.service.pushui.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class JobData {
	private String clientId;
	private int messageType;
	private String messageTitle;
	private String messageBody;
	private String userName;
	private String[] userNameList;
}
