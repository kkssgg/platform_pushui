package com.onaemo.tatteam.service.pushui.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
@Configuration
@EnableResourceServer
@Order(2)
public class Oauth2SecurityConfig extends ResourceServerConfigurerAdapter {

	@Value("security.oauth2.resource.key-value")
	String key;



	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
			.cors()
//		.and()
//			.anonymous()

		.and()
			.requestMatchers()
				.antMatchers("/rest/**")
		.and()
			.authorizeRequests()
				.anyRequest().authenticated()
		.and()
			.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
		.and()
			.csrf().disable()
		.headers().referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);

	}






	private AuthenticationEntryPoint authenticationEntryPoint() {
		return new AuthenticationEntryPoint() {
			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

				log.info("authenticationEntryPoint()");

				Authentication auth = SecurityContextHolder.getContext().getAuthentication();

				String ajaxHeader = request.getHeader("X-Requested-With");
				boolean isAjax = "XMLHttpRequest".equals(ajaxHeader);

				if(isAjax){
					response.sendError(HttpServletResponse.SC_FORBIDDEN, "Ajax Request Denied (Session Expired)");
				}else {
					response.sendRedirect("https://tatteam.onaem.com/onaemosuaa/oauth/authorize?response_type=code&client_id=onaemoclientId&redirect_uri=http://localhost:30011/callback");
				}

			}
		};
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {

		log.info("CorsConfigurationSource");
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.setAllowCredentials(true);
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		config.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}

}