package com.onaemo.tatteam.service.pushui.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter {

	private String securityName;


	private final String authHeader = "Authorization";

	public AuthenticationFilter(String securityName){
		this.securityName = securityName;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		final String authHeader = request.getHeader(this.authHeader);

		log.info("AuthenticationFilter :: " + request.getRequestURI());

		if(request.getRequestURI().startsWith("/rest")){
			if (authHeader != null && authHeader.startsWith("Bearer ")) {
				String token = authHeader.substring(7);
				Jwt jwt = JwtHelper.decode(token);

				ObjectMapper objectMapper = new ObjectMapper();
				Map<String, Object> claims = objectMapper.readValue(jwt.getClaims(), Map.class);

//				for (String key : claims.keySet()) {
//					log.info("claims [{}] :: {}", key, claims.get(key));
//				}

				List<GrantedAuthority> authorities = new ArrayList<>();
				authorities.add(new SimpleGrantedAuthority(claims.get("authorities").toString()));

				AuthVO authVO = new AuthVO();
				authVO.setClientId(claims.get("client_id").toString());
				authVO.setUsername(claims.get("user_name").toString());
				authVO.setName(claims.get("user_name").toString());
				authVO.setRole(claims.get("user_role").toString());

				authVO.setAuthorities(authorities);

				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(authVO, null, authorities);

				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(authentication);
			}

			if (!request.getMethod().equalsIgnoreCase("OPTIONS")) {
				chain.doFilter(request, response);
			}

		}else{
			chain.doFilter(request, response);
		}


	}
}