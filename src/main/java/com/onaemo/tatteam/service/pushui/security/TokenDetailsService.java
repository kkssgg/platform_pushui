package com.onaemo.tatteam.service.pushui.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class TokenDetailsService implements UserDetailsService {
	@Override
	public UserDetails loadUserByUsername(String token) throws UsernameNotFoundException {
		AuthVO authVO = new AuthVO();

		ObjectMapper objectMapper = new ObjectMapper();
		Jwt jwt = JwtHelper.decode(token);

		try {
			Map<String, Object> claims = objectMapper.readValue(jwt.getClaims(), Map.class);

			for (String key : claims.keySet()) {
				log.info("claims [{}] :: {}", key, claims.get(key));
			}

			List<GrantedAuthority> authorities = new ArrayList<>();


			authorities.add(new SimpleGrantedAuthority("ROLE_SERVER"));

			authVO.setClientId(claims.get("client_id").toString());
			authVO.setUsername(claims.get("user_name").toString());
			authVO.setName(claims.get("user_name").toString());
			authVO.setRole(claims.get("user_role").toString());

			authVO.setAuthorities(authorities);

		}catch (Exception e){
			log.error("TokenDetailsService Error " , e);
			throw new UsernameNotFoundException("User was not found in the database");
		}
		return authVO;
	}
}
