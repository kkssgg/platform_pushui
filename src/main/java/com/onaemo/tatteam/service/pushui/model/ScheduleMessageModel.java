package com.onaemo.tatteam.service.pushui.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleMessageModel {

	private String clientId;
	private String cronExpression;
	private Integer repeatTime;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	private Date startTime;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	private Date endTime;
	private String jobGroup;
	private String jobName;
	private Integer scheduleJobId;

	@JsonProperty("isCronJob")
	private boolean isCronJob;
	private Integer jobClassId;

	IndividualMessageModel individualMessageModel;

	MulticastMessageModel multicastMessageModel;

	TopicMessageModel topicMessageModel;
}
