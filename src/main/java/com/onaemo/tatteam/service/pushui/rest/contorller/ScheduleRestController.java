package com.onaemo.tatteam.service.pushui.rest.contorller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onaemo.tatteam.service.pushui.annotation.JwtClientId;
import com.onaemo.tatteam.service.pushui.annotation.JwtCustom;
import com.onaemo.tatteam.service.pushui.db.service.OnaemoPlatformProjectService;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import com.onaemo.tatteam.service.pushui.model.JobClass;
import com.onaemo.tatteam.service.pushui.model.ScheduleMessageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Slf4j
@RestController("rest.schedule.controller")
public class ScheduleRestController {

	@Value("${service.push.url}") String PUSH_SERVICE_URL;

	@Autowired
	OnaemoPlatformProjectService onaemoPlatformProjectService;

	/**
	 * @brief 플렛폼 PUSH 예약 이력 목록
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @return
	 */
	@GetMapping("/rest/push/schedule/list")
	public ResponseEntity<Map<String, Object>> getScheduleList(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                           @RequestParam("projectId") String projectId){

		Map<String, Object> result = new HashMap<>();
		try{


			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);

			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);
			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/schedule/list", HttpMethod.POST, httpEntity, String.class);

			if(responseEntity.getStatusCode() == HttpStatus.OK){
				log.info(responseEntity.getBody().toString());

				List<Map<String, Object>> data = objectMapper.readValue(responseEntity.getBody().toString(), List.class);
				result.put("items",data);
				return new ResponseEntity<>(result, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(responseEntity.getStatusCode());
			}

		}catch(Exception e){
			log.error("ERROR /rest/push/schedule/list", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @brief 플렛폼 PUSH 예약 이력 상세조회
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @param jobGroup
	 * @param jobName
	 * @return
	 */
	@GetMapping("/rest/push/schedule/info")
	public ResponseEntity<Map<String, Object>> getScheduleInfo(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                           @RequestParam("projectId") String projectId, @RequestParam("jobGroup") String jobGroup, @RequestParam("jobName") String jobName){

		Map<String, Object> result = new HashMap<>();
		try{


			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);

			Map<String, Object> requestBody = new HashMap<>();
//			requestBody.put("clientId",project.getClient_id());
			requestBody.put("clientId","onaemoclientId");
			requestBody.put("jobGroup",jobGroup);
			requestBody.put("jobName",jobName);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);
			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/schedule/list", HttpMethod.POST, httpEntity, String.class);

			if(responseEntity.getStatusCode() == HttpStatus.OK){
				log.info(responseEntity.getBody().toString());

				List<Map<String, Object>> data = objectMapper.readValue(responseEntity.getBody().toString(), List.class);
				result.put("items",data);
				return new ResponseEntity<>(result, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(responseEntity.getStatusCode());
			}

		}catch(Exception e){
			log.error("ERROR /rest/push/schedule/list", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * @brief 플랫폼 PUSH 스케쥴 제어 요청
	 * @param jwtCustom
	 * @param token	 *
	 * @param jobGroup
	 * @param jobName
	 * @param action
	 * @return
	 */
	@PostMapping("/rest/push/schedule/action")
	public ResponseEntity<Map<String, Object>> scheduleAction(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                          @RequestParam("jobGroup") String jobGroup, @RequestParam("jobName") String jobName,
	                                                          @RequestParam("action") String action ){
		Map<String, Object> result = new HashMap<>();
		try{
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			Map<String, Object> requestBody = new HashMap<>();
			requestBody.put("action",action);
			requestBody.put("jobGroup",jobGroup);
			requestBody.put("jobName",jobName);


			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String body = objectMapper.writeValueAsString(requestBody);
			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);


			try{
				ResponseEntity<String> responseEntity = restTemplate.exchange(PUSH_SERVICE_URL+"/onaemomessage/api/v1/schedule/action", HttpMethod.POST, httpEntity, String.class);

				if(responseEntity.getStatusCode() == HttpStatus.OK){
					return new ResponseEntity<>(result, HttpStatus.OK);
				}else{
					return new ResponseEntity<>(responseEntity.getStatusCode());
				}
			}catch (HttpStatusCodeException he){
				log.info(he.getStatusCode() +" \n " + he.getResponseBodyAsString());
				return new ResponseEntity<>(he.getStatusCode());
			}


		}catch(Exception e){
			log.error("/rest/push/schedule/action", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @brief 예약 PUSH 전송
	 * @param jwtCustom
	 * @param token
	 * @param projectId
	 * @param scheduleMessageModel
	 * @return
	 */
	@PostMapping("/rest/push/schedule/send")
	public ResponseEntity<Map<String, Object>> scheduleSend(@JwtClientId JwtCustom jwtCustom, @RequestHeader("Authorization") String token,
	                                                          @RequestParam("projectId") String projectId, @RequestParam("mode") String mode, @RequestBody ScheduleMessageModel scheduleMessageModel) {

		Map<String, Object> result = new HashMap<>();

		log.info("schedule send mode :: " + mode);
		try {
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();



			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);


//			scheduleMessageModel.getIndividualMessageModel().setClientId(project.getClient_id());
//			scheduleMessageModel.setClientId(project.getClient_id());

			if(mode.equals("add")){
				scheduleMessageModel.setClientId("onaemoclientId");
				if (scheduleMessageModel.getIndividualMessageModel() != null){
					scheduleMessageModel.getIndividualMessageModel().setClientId("onaemoclientId");
				}else if(scheduleMessageModel.getMulticastMessageModel() != null){
					scheduleMessageModel.getMulticastMessageModel().setClientId("onaemoclientId");
				}else if(scheduleMessageModel.getTopicMessageModel() != null){
					scheduleMessageModel.getTopicMessageModel().setClientId("onaemoclientId");
				}

				scheduleMessageModel.setJobGroup(project.getClient_id());
				scheduleMessageModel.setJobName(UUID.randomUUID().toString());
			}else{
				log.info("scheduleMessageModel.getClientId() :: "+scheduleMessageModel.getClientId());
				if (scheduleMessageModel.getIndividualMessageModel() != null){
					log.info("scheduleMessageModel.getIndividualMessageModel().getClientId() :: "+scheduleMessageModel.getIndividualMessageModel().getClientId());
				}else if(scheduleMessageModel.getMulticastMessageModel() != null){
					log.info("scheduleMessageModel.getMulticastMessageModel().getClientId() :: "+scheduleMessageModel.getMulticastMessageModel().getClientId());
				}else if(scheduleMessageModel.getTopicMessageModel() != null){
					log.info("scheduleMessageModel.getTopicMessageModel().getClientId() :: "+scheduleMessageModel.getTopicMessageModel().getClientId());
				}
				log.info(scheduleMessageModel.getJobGroup());
				log.info(scheduleMessageModel.getJobName());

			}

			Map<String, Object> requestBody = new HashMap<>();

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setBearerAuth(token.substring(7));
			httpHeaders.setAccept(Arrays.asList(MediaType.ALL));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			String body = objectMapper.writeValueAsString(scheduleMessageModel);

			HttpEntity<String> httpEntity = new HttpEntity<String>(body, httpHeaders);

			try {
				ResponseEntity<Object> responseEntity = null;
				if(mode.equals("add")){
					responseEntity = restTemplate.exchange(PUSH_SERVICE_URL + "/onaemomessage/api/v1/schedule/message/add", HttpMethod.POST, httpEntity, Object.class);
				}else{
					responseEntity = restTemplate.exchange(PUSH_SERVICE_URL + "/onaemomessage/api/v1/schedule/message/update", HttpMethod.POST, httpEntity, Object.class);
				}

				if (responseEntity.getStatusCode() == HttpStatus.OK) {
					return new ResponseEntity<>(result, HttpStatus.OK);
				} else {
					log.info(responseEntity.getStatusCode() + " \n " + responseEntity.getBody().toString());
					Map<String, Object> errorMap = new HashMap<>();
					errorMap.put("message", responseEntity.getBody().toString());
					return new ResponseEntity<>(errorMap, responseEntity.getStatusCode());
				}
			} catch (HttpStatusCodeException he){
				log.info("HttpStatusCodeException :: "+he.getStatusCode() +" \n " + he.getResponseBodyAsString());

				Map<String, Object> errorMap = new HashMap<>();
				ObjectMapper mapper = new ObjectMapper();
				errorMap = mapper.readValue(he.getResponseBodyAsString(), Map.class);


//				errorMap.put("message", he.getResponseBodyAsString().toString());
				return new ResponseEntity<>(errorMap, he.getStatusCode());
			}

			} catch (Exception e) {
				log.error("/rest/push/schedule/send", e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

}