package com.onaemo.tatteam.service.pushui.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageUtils {

	public static Map<String, Object> getPageNumbers(int totalNum, int startNum, int scaleNum){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("totalNum", totalNum);
		map.put("startNum", startNum);
		map.put("scaleNum", scaleNum);
		map.put("count", (totalNum-startNum));

		int totalPage = 0;
		int pageNumShare = (totalNum/scaleNum);
		int pageNumRest = (totalNum%scaleNum);
		if (pageNumRest != 0){
			totalPage = pageNumShare + 1 ;
			map.put("lastNum", (pageNumShare * scaleNum));
		}else{
			totalPage = pageNumShare;
			map.put("lastNum", ((pageNumShare-1) * scaleNum));
		}
		map.put("totalPageNum", totalPage);

		int pagingNum = 5;

		// 다음페이지
		int nextNum = 0;
		int currentPage = (startNum/scaleNum + 1);
		map.put("currentPageNum", currentPage);
		if(currentPage % pagingNum == 0) nextNum = currentPage * scaleNum;
		else nextNum = ((currentPage/pagingNum+1)*pagingNum)*scaleNum;
		map.put("nextNum", nextNum);
		String isNextPage = "false";
		if(totalNum > nextNum){
			isNextPage = "true";
		}
		map.put("isNextPage", isNextPage);
		// 이전페이지
		int prevNum = 0;
		if(startNum > scaleNum) {
			prevNum = ((startNum/scaleNum + 1)/pagingNum)-1;
		}else{
			prevNum = 0;
		}
		prevNum = (prevNum*pagingNum)*scaleNum;
		map.put("prevNum", prevNum);
		String isPrevPage = "false";
		if(startNum >= pagingNum * scaleNum){
			isPrevPage = "true";
		}
		map.put("isPrevPage", isPrevPage);
		// 페이징목록
		int pages=0, nowPageNum =0, startPage=0;
		if((startNum/scaleNum+1)%pagingNum == 0) {
			startNum = startNum - scaleNum;
		}
		List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < pagingNum; i++){
			nowPageNum = (startNum/scaleNum)/pagingNum;
			if(i == 0) pages = nowPageNum*pagingNum+1;
			startPage = (nowPageNum*pagingNum +i)*scaleNum;
			if(totalNum > startPage) {
				Map<String, Object> p = new HashMap<String, Object>();
				p.put("startNum", startPage);
				p.put("pageNum", (pages+i));
				pageList.add(p);
			}
		}
		map.put("pages", pageList);
		return map;
	}

}
