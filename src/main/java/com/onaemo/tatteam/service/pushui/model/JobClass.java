package com.onaemo.tatteam.service.pushui.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobClass {
	private String className;
	private String description;
	private String friendlyName;
	private int jobClassId;
}
