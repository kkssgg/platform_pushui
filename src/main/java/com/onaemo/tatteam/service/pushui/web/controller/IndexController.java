package com.onaemo.tatteam.service.pushui.web.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Slf4j
@Controller
public class IndexController {

	/**
	 * @param auth
	 * @param token
	 * @return
	 * @brief Index Page
	 */
	@GetMapping("/")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Authentication auth, @RequestParam(value = "Authorization", required = false, defaultValue = "") String token) {
		log.info("IndexController index() " + token);
		ModelAndView mv = new ModelAndView("index");
		return mv;

	}

	@GetMapping("/oauth/login")
	public String oauthLogin() {
		return "redirect:https://tatteam.onaemo.com/onaemosuaa/oauth/authorize?response_type=code&client_id=onaemoclientId&redirect_uri=http://localhost:30011/callback";
	}

	@GetMapping("/login")
	public ModelAndView login(Authentication auth, HttpServletRequest request, HttpServletResponse response, @RequestParam(value="Authorization", required = false, defaultValue = "") String token, RedirectAttributes redirectAttributes){

		log.info("IndexController login() token :: " + token);

		if(request.getHeader("Authorization") != null){
			log.info("Header => Authorization " + request.getHeader("Authorization"));
		}

		response.setHeader("Authorization", token);

		try {
			if (StringUtils.isEmpty(token)) {
				log.info("redirect ==> /");
				return new ModelAndView("redirect:/");
			} else {
				log.info("ModelAndView ==> login");
				return new ModelAndView("redirect:/?Authorization=" + token);

			}
		}catch(Exception e){
			log.error("IndexController login ", e);
		}


		return null;
	}


	/**
	 * @brief Authentication Server Callback
	 * @param auth
	 * @param request
	 * @param response
	 * @param code
	 * @return
	 */
	@GetMapping("/callback")
	public	ModelAndView callBackRequest(Authentication auth, HttpServletRequest request, HttpServletResponse response, @RequestParam("code")String code) {
		log.info("/callback");
		log.info("refer " + request.getHeader("referer"));

		MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		map.add("grant_type","authorization_code");
		map.add("client_id","onaemoclientId");
		map.add("redirect_uri","http://localhost:30011/callback");
		map.add("code", code);



		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("onaemoclientId","tatteam@798184"));
		ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://tatteam.onaemo.com/onaemosuaa/oauth/token", map, String.class);
		if(responseEntity.getStatusCode() != HttpStatus.OK) {
			throw new ResponseStatusException(responseEntity.getStatusCode(),"오내모 인증토큰 발급 실패");
		}

		Gson gson = new Gson();
		Map<String, Object> token = gson.fromJson(responseEntity.getBody().toString(), Map.class);

		response.setHeader("Authorization", "Bearer " + token.get("access_token"));
		ModelAndView mv = new ModelAndView("redirect:/login");
		mv.addObject("Authorization", token.get("access_token"));

		return mv;
	}

	@PostMapping("/oauth/user/info")
	@ResponseBody
	public ResponseEntity<String> oauthUserMe(@RequestHeader("Authorization") String token){
		try{
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> responseEntity = restTemplate.getForEntity("https://tatteam.onaemo.com/onaemosuaa/user/me",String.class);
			if(responseEntity.getStatusCode() == HttpStatus.OK){
				log.info(responseEntity.getBody()+"");
				return new ResponseEntity<String>("USER_INFO OK",HttpStatus.OK);
			}else{
				return new ResponseEntity<String>(responseEntity.getBody().toString(),responseEntity.getStatusCode());
			}

		}catch(Exception e){
			log.error("oauth user me",e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	};

	@PostMapping("/oauth/user/info2")
	@ResponseBody
	public ResponseEntity<String> oauthUserMe2(@RequestHeader("Authorization") String token){
		try{
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> responseEntity = restTemplate.getForEntity("https://tatteam.onaemo.com/onaemosuaa/user/me",String.class);
			if(responseEntity.getStatusCode() == HttpStatus.OK){
				log.info(responseEntity.getBody()+"");
				return new ResponseEntity<String>("USER_INFO OK",HttpStatus.OK);
			}else{
				return new ResponseEntity<String>(responseEntity.getBody().toString(),responseEntity.getStatusCode());
			}

		}catch(Exception e){
			log.error("oauth user me",e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	};


	/**
	 * @param auth
	 * @param request
	 * @param path
	 * @return
	 * @brief 페이지 이동
	 */
	@GetMapping(value = {"/onaemo/**"})
	public ModelAndView getView(Authentication auth, HttpServletRequest request, @RequestParam(value = "path", required = false, defaultValue = "") String path) {
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("path", request.getRequestURI());
		return mv;
	}

}
