package com.onaemo.tatteam.service.pushui.rest.contorller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.ribbon.proxy.annotation.Http;
import com.onaemo.tatteam.service.pushui.annotation.JwtClientId;
import com.onaemo.tatteam.service.pushui.annotation.JwtCustom;
import com.onaemo.tatteam.service.pushui.db.service.OnaemoPlatformProjectService;
import com.onaemo.tatteam.service.pushui.db.vo.OnaemoPlatformProject;
import com.onaemo.tatteam.service.pushui.utils.PageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController("rest.project.controller")
public class ProjectRestController {

	@Autowired
	OnaemoPlatformProjectService onaemoPlatformProjectService;

	/**
	 * @brief 플랫폼 프로젝트 목록
	 * @param jwtCustom
	 * @param startNum
	 * @return
	 */
	@GetMapping("/rest/platform/project/list")
	public ResponseEntity<Map<String, Object>> getProjectList(@JwtClientId JwtCustom jwtCustom, @RequestParam(value = "startNum", defaultValue = "0") int startNum){
		Map<String, Object> result = new HashMap<>();
		try{

			Map<String, Object> params = new HashMap<>();
			params.put("userId", jwtCustom.getUserId());

			int totalNum = onaemoPlatformProjectService.getTotalCount(params);
			int scaleNum = 7;
			int endNum = 0;
			if(totalNum < scaleNum) endNum = totalNum;
			else endNum = scaleNum;

			params.put("startNum", startNum);
			params.put("endNum", endNum);

			List<OnaemoPlatformProject> items = onaemoPlatformProjectService.getOnaemoPlatformProjectList(params);

			if (items.size() >= 0) {
				result.put("items", items);
				result.put("paging", PageUtils.getPageNumbers(totalNum, startNum, scaleNum));
				return new ResponseEntity<>(result,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}


		}catch(Exception e){
			log.error("/rest/platform/project/list", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @brief 플랫폼 프로젝트 정보
	 * @param jwtCustom
	 * @param projectId
	 * @return
	 */
	@GetMapping("/rest/platform/project/info")
	public ResponseEntity<Map<String, Object>> getProjectInfo(@JwtClientId JwtCustom jwtCustom, @RequestParam(value = "projectId") String projectId){
		Map<String, Object> result = new HashMap<>();
		try{

			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper objectMapper = new ObjectMapper();

			OnaemoPlatformProject project = onaemoPlatformProjectService.getOnaemoPlatformProject(projectId);

			result.put("projectName", project.getProjectName());

			if(project != null){
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

		}catch(Exception e){
			log.error("/rest/platform/project/info", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


}
